/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc;

import com.mycompany.albumproject.model.ReportSale;
import com.mycompany.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author Acer
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<ReportSale> reportDay = reportService.getReportSaleByDay();
        
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }
        
    }
}
